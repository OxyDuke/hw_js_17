const student = {
  name: "",
  lastName: "",
  subjects: {},
};

student.name = prompt("Введіть ім'я студента:");
student.lastName = prompt("Введіть прізвище студента:");

while (true) {
  const subjectName = prompt("Введіть назву предмета:");
  if (subjectName === null) {
    break;
  }

  const grade = parseFloat(prompt("Введіть оцінку за цей предмет:"));
  if (!isNaN(grade)) {
    student.subjects[subjectName] = grade;
  } else {
    alert("Будь ласка, введіть коректну оцінку.");
  }
}

let poorGradesCount = 0;
let totalGrade = 0;
let subjectsCount = 0;

for (const subject in student.subjects) {
  const grade = student.subjects[subject];
  totalGrade += grade;
  subjectsCount++;

  if (grade < 4) {
    poorGradesCount++;
  }
}

const averageGrade = Math.round(totalGrade / subjectsCount);

if (poorGradesCount === 0) {
  alert("Студент переведено на наступний курс.");
  if (averageGrade > 7) {
    alert("Студенту призначено стипендію.");
  }
} else {
  alert(
    `Студент отримав ${poorGradesCount} поганих оцінок. Середній бал: ${averageGrade}`
  );
}

console.log("Табель студента:", student.subjects);
